## Name
Covid-19 - Understanding which symptoms indentify a positive individual

## Description
To understand the difference between varying point trajectories and a singular point analysis of data we utilized a COVID-19 dataset. Our analysis focused on determining which symptoms were most effective in identifying COVID-19 positive individuals. 

## Documentation
The code is written in python, and each block of code is entitled and commented thoroughly.
